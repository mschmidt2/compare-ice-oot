#!/usr/bin/perl
use strict;
use warnings;

use File::Basename;
use File::Spec::Functions 'catfile';
use Data::Dumper;

my @dirmap = (
	# Header files in their special locations
	[qr#iidc\.h#,           'include/linux/net/intel/iidc.h'],
	[qr#virtchnl\.h#,       'include/linux/avf/virtchnl.h'],
	# Clearly OOT-specific infrastructure. To ignore.
	[qr#kcompat.*#,         ''],
	[qr#auxiliary.*#,       ''],
	[qr#common.mk#,         ''],
	[qr#linux#,             ''],
	[qr#Makefile#,          ''],
	[qr#Module\.supported#, ''],
	# Everything else is likely the driver sources proper.
	[qr#(.*)#,              'drivers/net/ethernet/intel/ice/%1$s'],
);

sub usage_and_exit() {
	print "Usage: $0 <OOT-ice-dir> <kernel-src-root-dir>\n";
	exit(1);
}

(@ARGV == 2) or usage_and_exit();
my ($oot_dir, $kernel_dir) = @ARGV;

my @paired_files;
my @oot_only_files;
my @ignored_files;

my $src_dir = catfile($oot_dir, 'src');
my @oot_files = <$src_dir/*>;
foreach my $oot_file (@oot_files) {
	my $file = basename($oot_file);
	my $matched = 0;
	foreach my $pair (@dirmap) {
		my ($from, $to) = @$pair;
		$file =~ /^$from$/ or next;
		$matched = 1;

		if ($to eq '') {
			push(@ignored_files, $file);
			last;
		}

		my $kernel_file = (@{^CAPTURE} ? sprintf($to, @{^CAPTURE}) : $to);
		my $kernel_path = catfile($kernel_dir, $kernel_file);

		if (-f $kernel_path) {
			push(@paired_files, [$file, $kernel_path]);
		} else {
			push(@oot_only_files, [$file, '/dev/null']);
		}
		last;
	}
	die if (!$matched);
}

print "Files with an in-tree sibling:\n";
print "  $$_[0]\n" for @paired_files;

print "\nFiles only in OOT:\n";
print "  $$_[0]\n" for @oot_only_files;

print "\nIrrelevant files in OOT:\n";
print "  $_\n" for @ignored_files;

#print Dumper(\@paired_files);
#print Dumper(\@oot_only_files);
#print Dumper(\@ignored_files);

print "\nFull diffs follow:\n==================\n";

for my $pair (@paired_files,@oot_only_files) {
	my $diff;
	open($diff, "-|", "diff", "-up", $$pair[1], catfile($src_dir, $$pair[0])) or die;
	while (<$diff>) {
		print $_;
	}
	close ($diff);
}
