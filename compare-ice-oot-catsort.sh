#!/bin/bash

if [ $# != 2 ]; then
	echo "Usage: $0 <kernel-src-dir> <ice-OOT-dir>"
	exit 1
fi

kernel_dir="$1"
oot_dir="$2"

line_count_tmpfile=$(mktemp --tmpdir catsortXXXXXXXXXX)

IFS=, read inserted deleted dummy <<EOF
$(diff -u \
<( {
	printf "$kernel_dir/include/linux/net/intel/iidc.h\0"
	printf "$kernel_dir/include/linux/avf/virtchnl.h\0"
	find "$kernel_dir/drivers/net/ethernet/intel/ice" -name '*.[ch]' -print0
   } | xargs -0 cat | sort) \
<(find "$oot_dir" \( -name 'kcompat*' -o -name 'auxiliary*' \) -prune -o \( -name '*.[ch]' -print0 \) \
     | xargs -0 cat | sort | tee >(wc -l > "$line_count_tmpfile") ) \
| diffstat -t | tail -n1)
EOF

total_oot_lines=$(<"$line_count_tmpfile")
rm "$line_count_tmpfile"

printf "Diff from in-tree to OOT:\n"
printf "    Inserted:       %10s\n" $inserted
printf "    Deleted:        %10s\n" $deleted
printf "Total lines in OOT: %10s\n" $total_oot_lines
echo "Relative cat-sort-diff metric: $((100*($inserted+$deleted)/$total_oot_lines)) %"
